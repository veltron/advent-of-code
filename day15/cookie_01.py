import itertools

# Create combinations of the four ingedients to total 100
def partitions(n, k):
    for c in itertools.combinations(range(n+k-1), k-1):
        yield [b-a-1 for a, b in zip((-1,)+c, c+(n+k-1,))]

ingredients = []

# Open the indredients properties and load into the ingredients list
text = open('input.txt')
for line in text:
	l = line.split(' ')
	ingredients.append([l[2][:-1], l[4][:-1], l[6][:-1], l[8][:-1]])
	
high = 0

# Loop through every combination of ingredients
for p in partitions(100, 4):	

	i = 0
	sub_totals = []
	
	# Loop through the igredients
	for ingredient in ingredients:			
		
		# Calculate the value x quantity for each property of the ingredient
		j = 0
		for value in ingredient:
			
			# Add value to overall running total of the property
			if i == 0:			
				sub_totals.append(p[i] * int(value))
			else:
				sub_totals[j] += p[i] * int(value)
			
			j += 1			
		i += 1
	
	total = 0
	j = 0
	
	# Work out the cookies overall value
	for property in sub_totals:
	
		# Disregard any combiantions with 0 or neagtive totals for a property	
		if property < 1:
			total = 0
			break
			
		if j == 0:
			total = property
		else:
			total = total * property
			
		j += 1
			
	# If the cookies value is higher than the current highest value then
	# set its value to the highest
	if total > high:
		high = total		

print high