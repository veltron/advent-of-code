import numpy as np

def turn_on(x, y):
	grid[x][y] +=  1
	return 1

def turn_off(x, y):
	if get_brightness(x,y) > 0:
		grid[x][y] -= 1
		return -1
        return 0

def toggle(x, y):
	grid[x][y] += 2
	return 2

def get_brightness(x, y):
	return grid.item((x, y))

# Function that works out each coord within the range and passes it to be
# turned on/off/toggled
# Returns the number increase in brightness (or decrease as a negative)
def process(start, end, instruction):
	lit = 0
	done = False
	x = int(start[0])
	y = int(start[1])
	while not done:
		if instruction == 1:
			lit += turn_on(x, y)
		elif instruction == 2:
			lit += turn_off(x, y)
		elif instruction == 3:
			lit += toggle(x, y)

		if x == int(end[0]) and y == int(end[1]):
			done = True
		elif y == int(end[1]):
			x += 1
			y = int(start[1])
		else:
			y += 1

	return lit

# Matrix 1000x1000 full of zeroes
grid = np.zeros((1000,1000))
total = 0

with open('input.txt') as text:

	for line in text:

		# Get positions for end of first coord and start of second coord
		before_through = line.find('through') - 1
		after_through = line.find('through') + 8
		instruction = 0
		
		#Get end coord
		start = [0, 0]
		end = line[after_through:len(line) - 1].split(',')

		# Split string to get the start coord and set instructions
		if 'on' in line:
			start = coord = line[8:before_through].split(',')
			instruction = 1

		elif 'off' in line:
			start = line[9:before_through].split(',')
			instruction = 2

		elif 'toggle' in line:
			start = line[7:before_through].split(',')
			instruction = 3

		if instruction:
			total += process(start, end, instruction)

print "Total lit: " + str(total)