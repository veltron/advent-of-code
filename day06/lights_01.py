import numpy as np

def turn_on(x, y, force):
	if force or not is_lit(x, y):
		grid[x][y] = True
		return 1
	return 0

def turn_off(x, y, force):
	if force or is_lit(x, y):
		grid[x][y] = False
		return -1
	return 0

def toggle(x, y):
	if is_lit(x, y):
		return turn_off(x, y, True)
	else:
		return turn_on(x, y, True)

def is_lit(x, y):
	return grid.item((x, y))

# Function that works out each coord within the range and passes it to be
# turned on/off/toggled
# Returns the number lights been turned on (or off as a negative)
def process(start, end, instruction):
	lit = 0
	done = False
	x = int(start[0])
	y = int(start[1])
	while not done:
		if instruction == 1:
			lit += turn_on(x, y, False)
		elif instruction == 2:
			lit += turn_off(x, y, False)
		elif instruction == 3:
			lit += toggle(x, y)

		if x == int(end[0]) and y == int(end[1]):
			done = True
		elif y == int(end[1]):
			x += 1
			y = int(start[1])
		else:
			y += 1

	return lit

# Matrix 1000x1000 full of False
grid = np.zeros((1000,1000), dtype=bool)
total = 0

with open('input.txt') as text:

	for line in text:

		# Get positions for end of first coord and start of second coord
		before_through = line.find('through') - 1
		after_through = before_through + 9
		instruction = 0

		start = [0, 0]
		#Get end coord
		end = line[after_through:len(line) - 1].split(',')

		# Split string to get the start coord and set instructions
		if 'on' in line:
			start = coord = line[8:before_through].split(',')
			instruction = 1

		elif 'off' in line:
			start = line[9:before_through].split(',')
			instruction = 2

		elif 'toggle' in line:
			start = line[7:before_through].split(',')
			instruction = 3

		total += process(start, end, instruction)

print "Total lit: " + str(total)