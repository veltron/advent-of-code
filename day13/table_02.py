import itertools as it

text = open('input.txt')
changes = []
positions = []

for line in text:
	temp = line.split(' ')
	
	# Make losing hapiness negative numbers
	if 'lose' in line:
		temp[3] = '-' + temp[3]
		
	# Add the subject, object people and effect on subjects happiness
	changes.append([temp[0], temp[10][:-2], int(temp[3])])
	
	# Add every person that occurred to a list
	positions.append(temp[0])
	positions.append(temp[10][:-2])	
	
# Add myself to the arrangement plans
for p in set(positions):
	changes.append([p, 'Myself', 0])
	changes.append(['Myself', p, 0])

positions.append('Myself')

# Put list of all the people into a set to strip the duplicates
# Then permutate to get all possible arrangements
layouts = it.permutations(set(positions))

totals = []

# Go through every arrangement an calculate each persons happiness value
for l in layouts:
	total = 0
	j = 0
	complete = False
	
	while not complete:
	
		# To keep track of the values we have found for the subject
		left = False
		right = False
		
		for c in changes:	
		
			# Set who is sitting on left and right of subject
			if j == 0:
				lpos = l[len(l) - 1]
				rpos = l[j + 1]
			elif j == len(l) - 1:
				lpos = l[j - 1]
				rpos = l[0]
			else:
				lpos = l[j - 1]
				rpos = l[j + 1]			
				
			# Check person on the right
			if l[j] == c[0] and rpos == c[1]:
				total += c[2]
				right = True
				
			# Check the person on the left
			elif l[j] == c[0] and lpos == c[1]:
				total += c[2]
				left = True
				
			# When we have found both left and right go to next person
			if left and right:
				j += 1

				left = False
				right = False
				
				# When the entire arrangement is done add total to a list
				# and break for next arrangement
				if j == len(l):
					complete = True
					
					totals.append(total)
					break
			
print 'Max: ' + str(max(totals))