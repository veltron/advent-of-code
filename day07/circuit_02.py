# Go through all instructions to find 'number' -> wire
# statements and store the wire name and value
def find_alphas(lines):
	for line in lines:
		x = line.rstrip().split(' ')
		names.append(x)
		if len(x) == 3 and x[0].isdigit():
			known.append(x[2])
			values.append(int(x[0]))

# Edits an "alpha" wire to a new value
def inject_alpha(injectee, value):
	values[known.index(injectee)] = value

with open('input.txt') as text:

	names = []
	known = []
	values = []
	i = 0

	omega = 'a'
	find_alphas(text)
	done = False
	while not done:
	
		# Looping through the instructions. If the operands are not known or the result is known skip.
		# If operands are known perform calculation and store results
		for item in names:
			if not item[len(item) - 1] in known:

				if len(item) == 3 and item[0] in known:
					known.append(item[2])
					values.append(values[known.index(item[0])])

				elif 'NOT' in item:
					if item[1] in known:
						known.append(item[3])
						# Have to mask as it's no unsigned
						values.append(~values[known.index(item[1])] & 0xffff)

				else:
				
					# Handle the options with 2 operands
					if 'AND' in item:
						if item[0] in known and item[2] in known:
							known.append(item[4])
							values.append(values[known.index(item[0])] & values[known.index(item[2])])

						# Handle AND's which start with 1
						elif item[0].isdigit() and item[2] in known:
							known.append(item[4])
							values.append(int(item[0]) & values[known.index(item[2])])

					elif 'OR' in item:
						if item[0] in known and item[2] in known:
							known.append(item[4])
							values.append(values[known.index(item[0])] | values[known.index(item[2])])

					elif 'RSHIFT' in item:
						if item[0] in known:
							known.append(item[4])
							values.append(values[known.index(item[0])] >> int(item[2]))

					elif 'LSHIFT' in item:
						if item[0] in known:
							known.append(item[4])
							values.append(values[known.index(item[0])] << int(item[2]))

			if omega in known:
				i += 1

				# If first time then reset all stored values/names
				if i == 1:
					print omega + " is: " + str(values[known.index(omega)])
					out = values[known.index(omega)]
					names = []
					known = []
					values = []
					
					# Re-populate "alphas" and inject a -> b
					find_alphas(open('input.txt'))
					inject_alpha('b', out)

				if i == 2:
						print omega + " is: " + str(values[known.index(omega)])
						done = True