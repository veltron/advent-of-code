#Function to calculate the distance of a reindeer over a duration
def find_distance(speed, run_time, rest_time, time):

	# Calculates the reindeers "block" time, which is the time they can run for
	# combined with the time they need to rest after
	block_time = run_time + rest_time
	
	# Check if they have ran for more than 1 "block" if they have work out
	# the distance travelled
	if int(time) > block_time:	
		completed_block = int(time / block_time) * speed * run_time
	else:
		completed_block = 0
	
	# Find out if they are in the run or rest phase when the time is update
	# If they are resting then they have run their full allowance for a block
	# If they are still running then work out how lon they have been running
	# and how far they would have got
	if time % block_time > run_time:
		return completed_block + (run_time * speed)
	else:
		return completed_block + ((time % block_time) * speed)
		
target_time = 2503
points = []

# To calculate the points we simulate a race every second by calling
# find_distance for every integer up until the target_time
# Keeping track of who was furthest after each race
for i in range(1, target_time + 1):

	distance = []

	text = open('input.txt')
	for line in text:			
	
		items = line.split(' ')
		distance.append(find_distance(int(items[3]), int(items[6]), int(items[13]), i))
		if i == 1:
			points.append(0)
		
	j = 0
	top = max(distance)
	for d in distance:
		if d == top:
			points[j] += 1
				
		j += 1
		
print max(points)