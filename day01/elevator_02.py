text = open('input.txt').read()

# We start at floor 0
floor = 0

# i is an iterator from the emuneration
# c is character at the current position
for i, c in enumerate(text):
	# The two conditions to decide which floor the lift is on
	if c == '(':
		floor = floor + 1
	elif c == ')':
		floor = floor - 1

	# If the floor reaches -1 we add one to the iterator
	# (starts at 0), print it out and break the loop
	if floor == -1:
		print "Position of first instruction to reach -1: " + str(i + 1)
		break