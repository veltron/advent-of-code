#open file and put contents as a string into instructions
instructions = open('input.txt').read()

#import the Counter class from the collections module
from collections import Counter

#load the string into a Counter
c = Counter(instructions)

#Calculate the final floor and output to the console
print c['('] - c[')']