import re

nice = 0

with open('input.txt') as text:

	for line in text:

		ok = False

		# Check for a sandwich with one filling
		for s in range(0, len(line) - 2):
			if line[s] == line[s+2]:
				ok = True
				break

		if ok:
			ok = False
			
			# Check for a repeating pair
			for s in range(0, len(line) - 1):
				if line.count(line[s] + line[s+1]) > 1:
					ok = True
					break

			if ok:
				nice += 1

print nice