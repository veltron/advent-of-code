import re

nice = 0
vowels = ['a', 'e', 'i', 'o', 'u']
forbidden_list = ["ab", "cd", "pq", "xy"]
with open('input.txt') as text:

	for line in text:

		forbidden = False

		# Check for two characters in a row
		match = re.search(r'(\w)\1{1}', line)
		if match:

			v_count = 0
			# check for 3 vowels
			for vowel in vowels:
				v_count += line.count(vowel)

			if v_count >= 3:

				# Check for forbidden sequences
				for pair in forbidden_list:
					match = re.search(r'(?:' + pair + ')+', line)
					#print match
					if match:
						forbidden = True

				if not forbidden:
					nice += 1

print nice