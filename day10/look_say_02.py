import re

# Staring sequence
starter = '1113222113'

# Loop through 50 times
for i in range(50):

	said = ''
	
	# Find every number
	for m in re.finditer(r"(\d)(\1+)?", starter):

		# If it doesn't repeat set '1'
		if not m.groups()[1]:
			said += '1'
			
		# If it does repeat set to how many times it repeats
		else:
			said += str(len(m.groups()[1]) + 1) 
			
		# Add the actual number after the times it appeared
		said += m.groups()[0]
	starter = said
	
print len(starter)