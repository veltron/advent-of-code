total = 0

with open('input.txt') as text:

	for line in text:
		# Just in case the line was empty
		if line != "":
			x,y,z = line.split("x")

		# Convert from string to integer
		x = int(x)
		y = int(y)
		z = int(z)

		# Find two shortest dimensions for the shortest perimeter
		if x >= y and x >= z:
			total = total + 2 * (y + z)
		elif y >= x and y >= z:
			total = total + 2 * (x + z)
		elif z >= x and z >= y:
			total = total + 2 * (x + y)

		# Add the presents volume for the bow
		total = total + (x * y * z)

print "Ribbon Total: " + str(total)