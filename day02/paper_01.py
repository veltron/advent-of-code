total = 0

with open('input.txt') as text:

	for line in text:
		# Just in case the line was empty
		if line != "":
			x,y,z = line.split("x")

		# Convert from string to integer
		x = int(x)
		y = int(y)
		z = int(z)

		# Work out areas
		xy = x * y
		xz = x * z
		yz = y * z

		# Find smallest side for overlap and add to total
		if xy <= xz and xy <= yz:
			total = total + xy
		elif xz <= xy and xz <= yz:
			total = total + xz
		elif yz <= xy and yz <= xz:
			total = total + yz

		# Add double the areas total
		total = total + (2 * (xy + xz + yz))