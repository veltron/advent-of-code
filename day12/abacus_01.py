import re

text = open('input.txt')
total = 0

# Find all numbers in the file and total them
for line in text:
	if line.rstrip() != '':
		for m in re.findall(r"[\-]?\d+", line):
			total += int(m)
print total