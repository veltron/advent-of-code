import re
import json

text = open('input.txt')
total = 0

# Empty object if it contains red
def del_red(obj):
	if 'red' in obj.values():
		return {}
	else:
		return obj

for line in text:
	if line.rstrip() != '':
	
		# Load as json and hook to del_red on every object
		j = str(json.loads(line, object_hook=del_red))
		
		# Count the numbers left after the json was stripped
		for m in re.findall(r"[\-]?\d+", j):
			total += int(m)
print total