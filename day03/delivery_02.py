from collections import Counter

text = open('input.txt').read()
coords = []
coords.append([0, 0])
x = 0
y = 0
robo_x = 0
robo_y = 0

for i, c in enumerate(text):

	# If instruction position is even find new coord for santa
	if i % 2 == 0:
		if c == '^':
			y = y + 1
		elif c == 'v':
			y = y - 1
		elif c == '<':
			x = x - 1
		elif c == '>':
			x = x + 1
	
	# Find new coord for robo santa
	else:
		if c == '^':
			robo_y = robo_y + 1
		elif c == 'v':
			robo_y = robo_y - 1
		elif c == '<':
			robo_x = robo_x - 1
		elif c == '>':
			robo_x = robo_x + 1

	duplicate = False

	# Check if the coords have been visited before
	for item in coords:
		j = 0
		for column in item:

			if j == 0:
				stored_x = column
			else:
				stored_y = column

			j += 1
			
		
		if i % 2 == 0:
			if stored_x == x and stored_y == y:
				duplicate = True
		else:
			if stored_x == robo_x and stored_y == robo_y:
				duplicate = True

	# Add new coords to coord list
	if duplicate == False:
		if i % 2 == 0:
			coords.append([x, y])
		else:
			coords.append([robo_x, robo_y])

print "Houses visited: " + str(len(coords))