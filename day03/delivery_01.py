from collections import Counter

text = open('input.txt').read()
coords = []
coords.append([0, 0])
x = 0
y = 0

for c in text:

	# Interpret the instructions to movements on x and y
	if c == '^':
		y = y + 1
	elif c == 'v':
		y = y - 1
	elif c == '<':
		x = x - 1
	elif c == '>':
		x = x + 1

	duplicate = False
	
	# Check if the new coord exists in coords list
	for item in coords:
		j = 0
		for column in item:
			if j == 0:
				stored_x = column
			else:
				stored_y = column

			j += 1

		if stored_x == x and stored_y == y:
			duplicate = True
	
	# If its a new coord store it in the array
	if duplicate == False:
			coords.append([x, y])

print "Houses visited: " + str(len(coords))