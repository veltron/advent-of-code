import re

# Function to increment the password
def inc_password(p):
	
	# Put the chars in a list so we can work with them
	p = list(p)
	complete = False
	i = 1
	
	# Cycles through password from the end if it finds 'z' sets it to 'a' then moves to
	# the char before it
	while not complete:	
	
		# Check if char is 'z' then set to 'a' if not increment
		if p[len(p) - i] == 'z':
			p[len(p) - i] = 'a'

		# Increment non 'z' char
		else:
			p[len(p) - i] = chr(ord(p[len(p) - i]) + 1)	
			complete = True			
		i += 1	
		
	# Put list into string
	return ''.join(p)

# Starting password
password = 'vzbxkghb'

# Find valid password two times
for i in range(2):
	valid = False
	while not valid:
	
		# Increment password
		password = inc_password(password)
		
		# Make sure it doesn't contain the forbidden i,o,l chars
		if not (('i' in password) or ('o' in password) or ('l' in password)):
		
			# Chech it contains two or more repeating chars
			if len(re.findall(r"(\w)\1", password)) > 1:
				
				# Check for an 'abc' like pattern
				for j in range(len(password) - 3):
					current_char = ord(password[j])
					if (current_char == ord(password[j + 1]) - 1) and (current_char == ord(password[j + 2]) - 2):
						valid = True

print password