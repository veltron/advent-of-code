import itertools as it

text = open('input.txt')
distances = []
places = []


for line in text:
	temp = line.split(' ')
	
	# Add the origin, destination and distance to a list
	distances.append([temp[0], temp[2], int(temp[4])])
	
	# Add every location that occurred to a list
	places.append(temp[0])
	places.append(temp[2])

# Put array of all locations into a set to strip the duplicates
# Then permutate to get all possible routes
routes = it.permutations(set(places))

totals = []

# Go through every route and look up distances between each stop
# Add them together and append to an array
for r in routes:
	total = 0
	j = 0
	complete = False
	
	while not complete:
		for d in distances:
			
			# Check origin and destination againt current route leg
			if (r[j] == d[0] and r[j + 1] == d[1]) or (r[j + 1] == d[0] and r[j] == d[1]):
				total += d[2]
				j += 1

			if j == (len(r) - 1):
				complete = True
				totals.append(total)
				break	
				
print 'Min: ' + str(min(totals))