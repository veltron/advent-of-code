import hashlib

# Loop through first 6 chars of string to see if they are '0'
def sixZeroes(hash):
	for i in range(0, 6):
		if hash[i] != "0":
			return False
			
	return True

input = 'ckczppom'
count = 1

while True:
	md5 = hashlib.md5()
	md5.update(input + str(count))
	if sixZeroes(md5.hexdigest()):
		print "The answer is: " + str(count) + " - " + md5.hexdigest()
		break
	count = count + 1

