import hashlib

# Loop through first 5 chars of string to see if they are '0'
def fiveZeroes(hash):
	for i in range(0, 5):
		if hash[i] != "0":
			return False
			
	return True

input = 'ckczppom'
count = 1

while True:
	md5 = hashlib.md5()
	md5.update(input + str(count))
	if fiveZeroes(md5.hexdigest()):
		print "The answer is: " + str(count) + " - " + md5.hexdigest()
		break
	count = count + 1

