# Sort of cheated at this one, instead of performing the "unescaping" I
# just calculated how many escape chars would be removed if it was escaped

import re

with open('input.txt') as text:

	code = 0
	value = 0

	for line in text:
		t_code = len(line.rstrip())
		code += t_code
		dfind = 0
		
		# Default substitution for strings even without escapes
		subs = - 2
		
		# If there are escapes
		if "\\" in line:
			
			# Find all '\' characters and positions
			for m in re.finditer("\\\\", line):
			
				# dfind keeps track of whether it is '\\'
				if dfind > 1:
					dfind = 0				

				# Find all '\\' and positions
				for d in re.finditer("\\\\\\\\", line):
					
					if m.start() == d.start():
						dfind += 1
					if m.end() == d.end():
						subs += -1
						dfind += 1
						break
						
				# Only check for hex and normal escapes if not '\\'
				if dfind < 1:		
					
					if line[m.start() + 1] == 'x':
						subs += -3
					else:
						subs += -1	

		value += t_code + subs
					
	print 'Difference: ' + str(code - value)
