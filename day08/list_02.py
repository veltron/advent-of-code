# Again sort of cheated at this one, instead of performing the "unescaping" I
# just calculated how many more characters would be needed to escape

import re

with open('input.txt') as text:

	code = 0
	value = 0

	for line in text:
		t_code = len(line.rstrip())
		code += t_code
		dfind = 0
		
		# Default addition for strings even without escapes
		adds = 4
		
		# If there are escapes
		if "\\" in line:
			
			# Find all '\' characters and positions
			for m in re.finditer("\\\\", line):
			
				# dfind keeps track of whether it is '\\'
				if dfind > 1:
					dfind = 0				
				
				# Find all '\\' and positions
				for d in re.finditer("\\\\\\\\", line):
					
					if m.start() == d.start():
						dfind += 1
					if m.end() == d.end():
						adds += 2
						dfind += 1
						break
						
				# Only check for hex and normal escapes if not '\\'
				if dfind < 1:		
					
					if line[m.start() + 1] == 'x':
						adds += 1
					else:
						adds += 2	

		value += t_code + adds
					
	print 'Difference: ' + str(value - code)
